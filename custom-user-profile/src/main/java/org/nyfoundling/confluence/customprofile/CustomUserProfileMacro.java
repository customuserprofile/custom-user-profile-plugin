package org.nyfoundling.confluence.customprofile;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.BootstrapManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.UserDetailsManager;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.spring.container.ContainerManager;
import com.atlassian.user.User;

/*
 Copyright 2015 NY Foundling

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.

 */

public class CustomUserProfileMacro implements Macro {

    private XhtmlContent xhtmlUtils;
    private SettingsManager settingsManager;
    private PermissionManager permissionManager;
    private BootstrapManager bootstrapManager;

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException {

        String username = parameters.get("user");

        UserAccessor userAccessor = (UserAccessor) ContainerManager.getInstance().getContainerContext().getComponent("userAccessor");
        User user = userAccessor.getUserByName(username);

        if (user == null) {
            throw new MacroExecutionException("User " + username + " not found");
        }

        boolean useDefaultProfilePic = false;
        if (isConfluence60()) {
            boolean hasViewPermission = permissionManager.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.VIEW,
                    PermissionManager.TARGET_PEOPLE_DIRECTORY);
            if (!hasViewPermission) {
                useDefaultProfilePic = true;
            }
        }

        ConfluenceUser confluenceUser = userAccessor.getUserByName(username);

        UserDetailsManager userDetailsManager = (UserDetailsManager) ContainerManager.getInstance().getContainerContext()
                .getComponent("userDetailsManager");

        String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();

        Map macroContext = MacroUtils.defaultVelocityContext();

        macroContext.put("useDefaultProfilePic", useDefaultProfilePic);
        if (!useDefaultProfilePic) {
            String profilePictureHtml = getProfilePictureHtml(context, confluenceUser);
            macroContext.put("profilePictureHtml", profilePictureHtml);
        }

        macroContext.put("user", user);
        macroContext.put("username", username);
        macroContext.put("userDetailsManager", userDetailsManager);
        macroContext.put("baseUrl", baseUrl);

        boolean showEmail = getBoolParam(parameters.get("email"), true);
        boolean showPhone = getBoolParam(parameters.get("phone"), false);
        boolean showIM = getBoolParam(parameters.get("im"), false);
        boolean showWebsite = getBoolParam(parameters.get("website"), false);
        boolean showPosition = getBoolParam(parameters.get("position"), false);
        boolean showLocation = getBoolParam(parameters.get("location"), false);
        boolean showDepartment = getBoolParam(parameters.get("department"), false);

        macroContext.put("showEmail", showEmail);
        macroContext.put("showPhone", showPhone);
        macroContext.put("showIM", showIM);
        macroContext.put("showWebsite", showWebsite);
        macroContext.put("showPosition", showPosition);
        macroContext.put("showLocation", showLocation);
        macroContext.put("showDepartment", showDepartment);

        String result = VelocityUtils.getRenderedTemplate("templates/custom_user_profile_template.vm", macroContext);
        return result;
    }

    private boolean isConfluence60() {
        return Integer.parseInt(bootstrapManager.getBuildNumber()) > 7100;
    }

    private String getProfilePictureHtml(ConversionContext context, ConfluenceUser confluenceUser) throws MacroExecutionException {
        String profilePictureHtml;
        try {
            profilePictureHtml = xhtmlUtils.convertStorageToView("<ac:structured-macro ac:name='profile-picture'>"
                    + "<ac:parameter ac:name='User'><ri:user ri:userkey='" + confluenceUser.getKey() + "'/>"
                    + "</ac:parameter></ac:structured-macro>", context);
        } catch (Exception e) {
            throw new MacroExecutionException("Profile picture macro exeption", e);
        }
        return profilePictureHtml;
    }

    public static boolean getBoolParam(String param, boolean defaultValue) {
        if ("true".equalsIgnoreCase(param) || "false".equalsIgnoreCase(param)) {
            return Boolean.parseBoolean(param);
        }
        return defaultValue;
    }

    @Override
    public BodyType getBodyType() {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return OutputType.INLINE;
    }

    public void setXhtmlUtils(XhtmlContent xhtmlUtils) {
        this.xhtmlUtils = xhtmlUtils;
    }

    public void setSettingsManager(SettingsManager settingsManager) {
        this.settingsManager = settingsManager;
    }

    public void setPermissionManager(PermissionManager permissionManager) {
        this.permissionManager = permissionManager;
    }

    public void setBootstrapManager(BootstrapManager bootstrapManager) {
        this.bootstrapManager = bootstrapManager;
    }

}
